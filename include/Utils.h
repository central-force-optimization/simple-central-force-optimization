/*
 * Utils.h
 *
 *  Created on: Jan 11, 2010
 *      Author: rgreen
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <iostream>
#include <string>
#include <sqlite3.h> 

namespace CFO {
    namespace Utils {

        /******************************** Sqlite3 ********************************/
        extern int callback(void *NotUsed, int argc, char **argv, char **azColName);
        extern void setupDb();
        extern void recordValues(std::string functionName, double fitness, int bestProbe, int bestTimeStep, double Gamma, 
                                 int NpNd, int Np, int Nd, int lastStep, int Evals, int Corrections, double posTime, 
                                 double cortime, double fitTime, double accelTime, double shrinkTime, double convTime, double totalTime);
    }
}

#endif