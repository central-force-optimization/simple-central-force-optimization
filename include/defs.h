#ifndef DEFS_H_
#define DEFS_H_

#include <vector>


namespace CFO {

    typedef std::vector<int>             iArray1D;
    typedef std::vector<double>          Array1D;
    typedef std::vector<CFO::Array1D>    Array2D;  
    typedef std::vector<CFO::Array2D>    Array3D;

    //
    // Constants
    //
    static const double Pi    = 3.141592653589793238462643;
    static const double TwoPi = 2 * Pi;
    static const double e     = 2.718281828459045235360287;
}
#endif