#include "Utils.h"


namespace CFO{
    namespace Utils{
        int callback(void *NotUsed, int argc, char **argv, char **azColName){
            int i;
            for(i=0; i<argc; i++){
                printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
            }
            printf("\n");
            return 0;
        }

        void setupDb(){
            sqlite3 *db;
            char *zErrMsg = 0;
            int rc;
            std::string sql;

            /* Open database */
            rc = sqlite3_open("CFO.db", &db);
            if(rc){
                fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
                exit(0);
            }else{
                fprintf(stderr, "Opened database successfully\n");
            }

           /* Create SQL statement */
           sql = "CREATE TABLE IF NOT EXISTS Results("  \
                    "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," \
                    "Function           TEXT    NOT NULL," \
                    "Fitness            REAL    NOT NULL," \
                    "BestProbe          INT     NOT NULL,"\
                    "bestTimeStep       INT     NOT NULL,"\
                    "Gamma              REAL    NOT NULL,"\
                    "NpNd               INT     NOT NULL,"\
                    "Np                 INT     NOT NULL,"\
                    "Nd                 INT     NOT NULL,"\
                    "LastStep           INT     NOT NULL,"\
                    "Evals              INT     NOT NULL,"\
                    "Corrections        INT     NOT NULL,"\
                    "posTime            REAL    NOT NULL,"\
                    "corTime            REAL    NOT NULL,"\
                    "fitTime            REAL    NOT NULL,"\
                    "accelTime          REAL    NOT NULL,"\
                    "shrinkTime         REAL    NOT NULL,"\
                    "convTime           REAL    NOT NULL,"\
                    "totalTime          REAL    NOT NULL,"\
                    "insertedOn         TIMESTAMP DEFAULT CURRENT_TIMESTAMP);";

            rc = sqlite3_exec(db, sql.c_str(), callback, 0, &zErrMsg);
            
            if(rc != SQLITE_OK){
                fprintf(stderr, "SQL error: %s\n", zErrMsg);
                sqlite3_free(zErrMsg);
            }else{
                fprintf(stdout, "Table created successfully\n");
            }
            
            sqlite3_close(db);
        }

        void recordValues(std::string functionName, double fitness, int bestProbe, int bestTimeStep, double Gamma, int NpNd, int Np, int Nd, int lastStep, int Evals, int Corrections, double posTime, double cortime, double fitTime, double accelTime, double shrinkTime, double convTime, double totalTime){

            sqlite3 *db;
            char *zErrMsg = 0;
            int rc;
            std::string sql;

           /* Open database */
            rc = sqlite3_open("CFO.db", &db);
            if(rc){
              fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
              exit(0);
            }else{
                // cout << "Database Opened\n";
            }

           /* Create SQL statement */
            // sql = ;
            sql = "INSERT INTO RESULTS (id, Function, Fitness, bestProbe, bestTimeStep, Gamma, NpNd, Np, Nd, LastStep, Evals, Corrections, "\
                                      "posTime, corTime, fitTime, accelTime, shrinkTime, convTime, totalTime) ";    
            sql = sql + "VALUES (NULL, '";
            sql = sql + functionName                + "', ";
            sql = sql + std::to_string((long double)fitness)     + ", ";
            sql = sql + std::to_string((long long int)bestProbe)   + ", ";
            sql = sql + std::to_string((long long int)bestTimeStep)   + ", ";
            sql = sql + std::to_string((long double)Gamma)       + ", ";
            sql = sql + std::to_string((long long int)NpNd)        + ", ";
            sql = sql + std::to_string((long long int)Np)          + ", ";
            sql = sql + std::to_string((long long int)Nd)          + ", ";
            sql = sql + std::to_string((long long int)lastStep)    + ", ";
            sql = sql + std::to_string((long long int)Evals)       + ", ";
            sql = sql + std::to_string((long long int)Corrections)       + ", ";
            sql = sql + std::to_string((long double)posTime)     + ", ";
            sql = sql + std::to_string((long double)cortime)     + ", ";
            sql = sql + std::to_string((long double)fitTime)     + ", ";
            sql = sql + std::to_string((long double)accelTime)   + ", ";
            sql = sql + std::to_string((long double)shrinkTime)  + ", ";
            sql = sql + std::to_string((long double)convTime)    + ", ";
            sql = sql + std::to_string((long double)totalTime);
            sql = sql + ");";

           /* Execute SQL statement */
           rc = sqlite3_exec(db, sql.c_str(), callback, 0, &zErrMsg);
           if( rc != SQLITE_OK ){
              fprintf(stderr, "SQL error: %s\n", zErrMsg);
              sqlite3_free(zErrMsg);
           }else{
              // fprintf(stdout, "Records created successfully\n");
           }
           sqlite3_close(db);
        }
    }
}