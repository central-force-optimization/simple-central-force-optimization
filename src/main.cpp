
#include <iostream>
#include <iomanip>
#include <vector>
#include <math.h>
#include <cstdlib>
// #include <cmath>
#include <fstream>
#include <limits>
#include <omp.h>
#include <sstream>

#include "CStopWatch.h"
#include "defs.h"
#include "Utils.h"

using namespace std;
using namespace CFO;


double sigmoid(double x){
    return 1/(1+exp(-x));
}

// -5.12 <= x <= 5.12
double F1(Array3D& R, int Nd, int p, int j) { // Sphere, UniModal
    double Z=0,  Xi;

    for(int i=0; i<Nd; i++){
        Xi = R[p][i][j];
        Z = Z + pow(Xi,2);
    }
    return -Z;
}

// -5.12 <= x <= 5.12
double F2(Array3D& R, int Nd, int p, int j) { // Rastrigin, MultiModal
    double Z=0,  Xi;

    for(int i=0; i<Nd; i++){
        Xi = R[p][i][j];
        Z += (pow(Xi,2) - 10 * cos(2*Pi*Xi) + 10);
    }
    return -Z;
}

// -600 <= x <= 600
double F3(Array3D& R, int Nd, int p, int j) { // Griewank, MultiModal
    double Z, Sum, Prod, Xi;

    Z = 0; Sum = 0; Prod = 1;
    for(int i=0; i<Nd; i++){
        Xi = R[p][i][j];
        Sum  += Xi*Xi;
        Prod *= cos(Xi/sqrt((double)i)+1)/4000.0f; 
        
        #ifdef  _WIN32
            if(_isnan(Prod)) Prod = 1;
        #else
            if(isnan(Prod)) Prod = 1;
        #endif
    }
    
    Z = Sum - Prod;
    return -Z;
}


// -2.048 <= x <= 2.048
double F4(Array3D& R, int Nd, int p, int j) { // Rosenbrock, UniModal
    double Z=0, Xi, XiPlus1;

    for(int i=0; i<Nd-1; i++){
        Xi = R[p][i][j];
        XiPlus1 = R[p][i+1][j];
        Z = Z + pow(100 * pow(XiPlus1-pow(Xi,2),2)+(Xi-1),2);
        //Z +=  100 * pow(XiPlus1 - pow(Xi,2),2) + pow(Xi-1,2);
    }
    return -Z;
}

// -32.768 <= x <= 32.768
double F5(Array3D& R, int Nd, int p, int j){ // Ackley, Multimodal
    // -32 <= Xi <= 32
    double Z = 0;
    double term1 = 0, term2 = 0;
    for(int i = 0; i < Nd; i++){
        double Xi = R[p][i][j];
        term1 = term1 + pow(Xi, 2);
        term2 = term2 + cos(TwoPi * Xi);
    }
    term1 = -0.2 * sqrt( (1/Nd) * term1);
    term2 = (1/Nd) * term2;
    Z = -20 * pow(e, term1) - pow(e, term2) + 20 + e;
    
    return -Z;
}
double F6(Array3D& R, int Nd, int p, int j){ // Exponential, UniModal
    // -5.12 <= Xi <= 5.12
    double Z = 0;
    double sum = 0, sum_alpha = 0;
    for(int i = 0; i < Nd; i++){
        double Xi = R[p][i][j];
        sum = sum + pow(e, i * Xi) * i;
        sum_alpha = sum_alpha + pow(e, -5.12 * i);
    }
    Z = sum - sum_alpha;    
    return -Z;
}


double F7(Array3D& R, int Nd, int p, int j){ // Lunacek, MultiModal
    double Z = 0,       u1 = 2.5,       d   = 1;
    double func1 = 0,       func2 = 0,  term2 = 0;          
    double s  = 1 - (1/ (2*sqrt(Nd + 20) - 8.2));
    double u2 = -1*sqrt( ( pow(u1, 2) - d) / s );
        
    for(int i = 0; i < Nd; i++){
        double Xi = R[p][i][j];
        func1 = func1 + pow(Xi - u1, 2);
        func2 = func2 + pow(Xi - u2, 2);
        term2 = term2 + 1 - cos(TwoPi*(Xi - u1));
    }
    double term1 = std::min(func1, d*Nd + s*func2);
    Z = term1 + 10*term2;
    return -Z;
}

// -64 <= Xi <= 64  
double F8(Array3D& R, int Nd, int p, int j){ // Ridge
        
    double Z = 0;

    for(int i = 0; i < Nd; i++){
        double sum_inner = 0;

        for(int k = 0; k < i; k++){
            double Xi = R[p][k][j];
            sum_inner = sum_inner + Xi;
        }
        Z = Z + pow(sum_inner, 2);
    }
    return -Z;
}

// -100 <= Xi <= 100
double F9(Array3D& R, int Nd, int p, int j){ // Schaffer's F7, MultiModal
        
        double Z = 0;
        for(int i = 0; i < Nd-1; i++){
            double Xi = R[p][i][j];
            double Xi_plus1 = R[p][i+1][j];
            double Si = sqrt( pow(Xi, 2) + pow(Xi_plus1, 2));

            Z = Z + pow( (1/(Nd-1)) * sqrt(Si) * (sin(50.0*pow(Si, 0.2) + 1)), 2);
        }
        return -Z;
}

bool hasFitnessSaturated(int nSteps, int j, int Np, int Nd, Array2D& M, Array3D& R, int DiagLength) {

    double fitnessSatTol    = 0.000001;
    double bestFitness      = -numeric_limits<double>::infinity();
    double bestFitnessStepJ = -numeric_limits<double>::infinity();

    if (j < nSteps + 10)
        return false;

    double sumOfBestFitness = 0;

    for (int k = j - nSteps + 1; k <= j; k++) {
        bestFitness = -numeric_limits<double>::infinity();

        for (int p = 0; p < Np; p++) {

            if (M[p][k] >= bestFitness) {
                bestFitness = M[p][k];
            }
        }

        if(k == j){
            bestFitnessStepJ = bestFitness;
        }

        sumOfBestFitness += bestFitness;
    }

    if (fabs(sumOfBestFitness / nSteps - bestFitnessStepJ) <= fitnessSatTol) {
        return true;
    }

    return false;
}
double UnitStep(double X) {
    if (X < 0.0)
        return 0.0;

    return 1.0;
}

void GetBestFitness(Array2D& M, int Np, int stepNumber, double& bestFitness, int& bestProbeNumber, int& bestTimeStep){

    bestFitness = M[0][0];

    for(int i=0; i<stepNumber; i++){
        for(int p=0; p<Np; p++){
            if(M[p][i] >= bestFitness){
                bestFitness = M[p][i];
                bestProbeNumber = p;
                bestTimeStep = i;
            }
        }
    }
}

int getMaxProbes(int i){

    if(i >= 1 && i <= 6)       { return 14;}
    else if(i >= 7 && i <= 10) { return 12;}
    else if(i >= 11 && i <= 15){ return 10;}
    else if(i >= 16 && i <= 20){ return 8; }
    else if(i >= 21 && i <= 30){ return 6; }
    else                       { return 4; }
}


void MiniBenchmarkCFO(int& bestNp, int Nd, int& Nt, double& bestGamma, double oMin, double oMax, double(*objFunc)(Array3D& ,int, int, int), string functionName, bool printTrialResults=false, bool printResults=false, bool printPrettyResults=false) {

    vector<double> xMin;
    vector<double> xMax;
    double bestFitness = -numeric_limits<double>::infinity();
    double bestFitnessThisRun = -numeric_limits<double>::infinity();
    double bestFitnessOverall = -numeric_limits<double>::infinity();
    double Gamma, Frep;

    double deltaFrep = 0.1;
    double DeltaXi;
    double *bestCoords; 
    bestCoords = new double[Nd];
    double Denom, Numerator, SumSQ, DiagLength = 0;
    double Alpha = 1, Beta = 2;
    Array3D bestR;
    Array3D bestA;
    Array2D bestM;
    string s;
    stringstream ss;
    ofstream myFile;

    int lastStep, Np;
    int bestProbeNumber = 0, bestTimeStep = 0, bestNumEvals = 0;;
    int bestProbeNumberThisRun = 0, bestTimeStepThisRun = 0;
    int bestProbeNumberOverall = 0, bestTimeStepOverall = 0;
    int maxProbesPerDimension = getMaxProbes(Nd);
    double NumGammas = 21;
    int bestNpNd = 0;
    int lastStepBestRun = 0;
    int numEvals = 0;
    
    int numCorrections=0, bestNumCorrections=0;
    bool isCorrected;

    CStopWatch timer, timer1, timer2;
    double positionTime=0, correctionTime=0, fitnessTime=0, accelTime=0, shrinkTime=0, 
                convergeTime=0, totalTime=0, tTime = 0;
    
    string fileName;

    xMin.resize(Nd, oMin);
    xMax.resize(Nd, oMax);

    timer.startTimer();
    for(int numProbesPerAxis = 2; numProbesPerAxis <= maxProbesPerDimension; numProbesPerAxis += 2){
        Np = numProbesPerAxis*Nd;

        Array3D R(Np, Array2D(Nd, Array1D(Nt, 0)));
        Array3D A(Np, Array2D(Nd, Array1D(Nt, 0)));
        Array2D M(Np, Array1D(Nt, 0));

        for(int GammaNumber=1; GammaNumber <=NumGammas; GammaNumber++){
            Gamma = (GammaNumber-1)/(NumGammas-1);

            // Reset Matrices
            for(int j=0; j<Nt; j++){
                for (int p = 0; p < Np; p++) {
                    for (int i = 0; i < Nd; i++) {
                        R[p][i][j] = 0; A[p][i][j] = 0;
                    }
                    M[p][j] = 0;
                }
            }

            // Reset Bests
            bestFitness     = bestFitnessThisRun        =  -numeric_limits<double>::infinity();
            bestProbeNumber = bestProbeNumberThisRun    = 0;
            bestTimeStep    = bestTimeStepThisRun       = 0;
            numEvals       = 0;
            numCorrections = 0;
            
            positionTime = 0; correctionTime = 0; fitnessTime = 0;
            accelTime = 0;    shrinkTime = 0;     convergeTime = 0;
            // End Reset Values

            timer2.startTimer();
            for (int i = 0; i < Nd; i++) {
                DiagLength += pow(xMax[i] - xMin[i], 2);
            }
            DiagLength = sqrt(DiagLength);

            // Initial Probe Distribution
            for (int i = 0; i < Nd; i++) {
                for (int p = 0; p < Np; p++) {
                    R[p][i][0] = xMin[i] + Gamma * (xMax[i] - xMin[i]);
                }
            }
            for (int i = 0; i < Nd; i++) {
                DeltaXi = (xMax[i] - xMin[i]) / (numProbesPerAxis-1);
                int p;

                for (int k = 0; k < numProbesPerAxis; k++) {
                    p = k + numProbesPerAxis * i;
                    R[p][i][0] = xMin[i] + k * DeltaXi;
                }
            }

            // Set Initial Acceleration to 0
            for (int p = 0; p < Np; p++) {
                for (int i = 0; i < Nd; i++) {
                    A[p][i][0] = 0;
                }
            }
            // Compute Initial  Fitness
            for (int p = 0; p < Np; p++) {
                M[p][0] = objFunc(R, Nd, p, 0);
                numEvals++;
            }
            // Time Steps
            lastStep = Nt;
            bestFitnessThisRun = M[0][0];// -numeric_limits<double>::infinity();
            Frep = 0.5;

            for (int j = 1; j < Nt; j++) {
                // Compute new positions
                timer1.startTimer();
                for (int p = 0; p < Np; p++) {
                    for (int i = 0; i < Nd; i++) {
                        R[p][i][j] = R[p][i][j - 1] + A[p][i][j - 1];
                    }
                }
                timer1.stopTimer();
                positionTime += timer1.getElapsedTime();
                
                // Correct any errors
                timer1.startTimer();
                for (int p = 0; p < Np; p++) {
                    isCorrected = true;
                    for (int i = 0; i < Nd; i++) {

                        if (R[p][i][j] < xMin[i]) {
                            R[p][i][j] = max(xMin[i] + Frep * (R[p][i][j - 1] - xMin[i]), xMin[i]);
                            isCorrected = true;
                        }
                        if (R[p][i][j] > xMax[i]) {
                            R[p][i][j] = min(xMax[i] - Frep * (xMax[i] - R[p][i][j - 1]), xMax[i]);
                            isCorrected = true;
                        }
                    }
                    if(isCorrected){ numCorrections++;}
                }
                timer1.stopTimer();
                correctionTime += timer1.getElapsedTime();
                // Update Fitness
                timer1.startTimer();
                for (int p = 0; p < Np; p++) {
                    M[p][j] = objFunc(R, Nd, p, j);
                    numEvals++;

                    if(M[p][j] >= bestFitness){
                        bestFitness     = M[p][j];
                        bestTimeStep    = j;
                        bestProbeNumber = p;
                    }
                }

                if(bestFitness >= bestFitnessThisRun){
                    bestFitnessThisRun      = bestFitness;
                    bestProbeNumberThisRun  = bestProbeNumber;
                    bestTimeStepThisRun     = bestTimeStep;
                }
                timer1.stopTimer();
                fitnessTime += timer1.getElapsedTime();
                
                // Compute next Acceleration
                timer1.startTimer();
                for (int p = 0; p < Np; p++) {
                    for (int i = 0; i < Nd; i++) {

                        A[p][i][j] = 0;

                        for (int k = 0; k < Np; k++) {
                            if (k != p) {
                                SumSQ = 0.0;

                                for (int L = 0; L < Nd; L++) {
                                    SumSQ = SumSQ + pow(R[k][L][j] - R[p][L][j], 2);
                                }
                                if(SumSQ != 0){
                                    Denom = sqrt(SumSQ);
                                    Numerator = UnitStep((M[k][j] - M[p][j])) * (M[k][j]- M[p][j]);
                                    A[p][i][j] = A[p][i][j] + (R[k][i][j] - R[p][i][j]) * pow(Numerator,Alpha)/pow(Denom,Beta);
                                }
                            }
                        }
                    }
                }
                timer1.stopTimer();
                accelTime += timer1.getElapsedTime();
                // Adjust Frep
                Frep += deltaFrep;
                if(Frep > 1.0){
                     Frep = 0.05;
                }

                // Shrink Boundaries
                timer1.startTimer();
                if(j % 10 == 0 && j >= 20){
                    for(int i=0; i<Nd; i++){
                        xMin[i] = xMin[i] + (R[bestProbeNumber][i][bestTimeStep] - xMin[i])/2;
                        xMax[i] = xMax[i] - (xMax[i] - R[bestProbeNumber][i][bestTimeStep])/2;
                    }
                    // Correct any errors
                    for (int p = 0; p < Np; p++) {
                        isCorrected = true;
                        for (int i = 0; i < Nd; i++) {

                            if (R[p][i][j] < xMin[i]) {
                                R[p][i][j] = max(xMin[i] + Frep * (R[p][i][j - 1] - xMin[i]), xMin[i]);
                                isCorrected = true;
                            }
                            if (R[p][i][j] > xMax[i]) {
                                R[p][i][j] = min(xMax[i] - Frep * (xMax[i] - R[p][i][j - 1]), xMax[i]);
                                isCorrected = true;
                            }
                        }
                        if(isCorrected){ numCorrections++;}
                    }
                }
                timer1.stopTimer();
                shrinkTime += timer1.getElapsedTime();

                // Test for fitness saturation
                lastStep = j;
                timer1.startTimer();
                if (hasFitnessSaturated(25, j, Np, Nd, M, R, DiagLength)) {
                    timer1.stopTimer();
                    convergeTime += timer1.getElapsedTime();
                    break;
                }
                timer1.stopTimer();
                convergeTime += timer1.getElapsedTime();

            }//End Time Steps
            timer2.stopTimer();
            tTime = timer2.getElapsedTime();
            
            if(bestFitnessThisRun >= bestFitnessOverall){
                bestFitnessOverall      = bestFitnessThisRun;
                bestProbeNumberOverall  = bestProbeNumberThisRun;
                bestTimeStepOverall     = bestTimeStepThisRun;

                bestNumEvals        = numEvals;
                bestNumCorrections  = numCorrections;
                bestNpNd            = numProbesPerAxis;
                bestGamma           = Gamma;
                lastStepBestRun     = lastStep;
                bestNp              = Np;
            }

            //Reset Decision Space
            xMin.clear(); xMax.clear();
            xMin.resize(Nd, oMin);
            xMax.resize(Nd, oMax);
                    
            if(printTrialResults){
                if(printPrettyResults){
                    cout << "Function Name:             " << functionName << endl;
                    cout << "Best Fitness:              " << setprecision(18) << bestFitness << endl;
                    cout << "Best Probe #:              " << setprecision(0) << bestProbeNumber << endl;
                    cout << "Best Time Step:            " << bestTimeStep << endl;
                    cout << "Gamma:                     " << setprecision(18) <<  Gamma << endl;
                    cout << "Probes Per Axis:           " << numProbesPerAxis << endl;
                    cout << "Number of Probes:          " << Np << endl;
                    cout << "Last Step:                 " << setprecision(0) << lastStep << endl << endl;
                }else{
                    cout    << functionName         << " "
                            << bestFitness          << " " 
                            << bestProbeNumber      << " "
                            << bestTimeStep         << " "
                            << Gamma                << " "
                            << numProbesPerAxis     << " "
                            << Np                   << " "
                            << Nd                   << " "
                            << lastStep             << " "
                            << numEvals             << " "
                            << numCorrections       << " "
                            << positionTime         << " "
                            << correctionTime       << " "
                            << fitnessTime          << " "
                            << accelTime            << " "
                            << shrinkTime           << " "
                            << convergeTime         << " "
                            << tTime                << endl;
                }
            }
        }//End Gamma
    }//End NpNd
    timer.stopTimer();
    totalTime = timer.getElapsedTime();

    if(printResults){
   
        cout    << functionName             << " "
                << bestFitnessOverall       << " " 
                << bestProbeNumberOverall   << " "
                << bestTimeStepOverall      << " "
                << bestGamma                << " "
                << bestNpNd                 << " "
                << bestNp                   << " "
                << Nd                       << " "
                << lastStepBestRun          << " "
                << bestNumEvals             << " "
                << bestNumCorrections       << " "
                << totalTime                << endl;
    }
    Nt = lastStepBestRun+1;
}



void PrintResults(Array3D& R, Array3D& A, Array2D& M, Array1D bestProbeNumberArray, Array1D bestFitnessArray,int Np, int Nd, int lastStep, string functionName) {
    ofstream myFile;
    string fileName = functionName + "_R" + ".txt" ;
    myFile.open(fileName.c_str(), ios::trunc);
    for(int j=0; j<lastStep; j++){
        for(int p=0; p<Np; p++){
            for(int i=0; i<Nd; i++){
                myFile << j << "," << p+1 << "," << i+1 << "," << setprecision(15) << R[p][i][j] << endl;
            }
        }
    }
    myFile.close();
    fileName = functionName +"_A" + ".txt";
    myFile.open(fileName.c_str(), ios::trunc);
    for(int j=0; j<lastStep; j++){
        for(int p=0; p<Np; p++){
            for(int i=0; i<Nd; i++){
                myFile << j << "," << p+1 << "," << i+1 << "," << A[p][i][j] << endl;
            }
        }
    }
    myFile.close();
    fileName = functionName + "_M" + ".txt";
    myFile.open(fileName.c_str(), ios::trunc);
    for(int j=0; j<lastStep; j++){
        for(int p=0; p<Np; p++){
            myFile << j << "," << p+1 << "," << M[p][j] << endl;
        }
    }
    myFile.close();
    fileName = functionName + "_Bests" + ".txt";
    myFile.open(fileName.c_str(), ios::trunc);
    for(int j=0; j<lastStep; j++){
        myFile << j << "," << bestProbeNumberArray[j] << "," << bestFitnessArray[j] << endl;
    }
    myFile.close();
}

void standardCFO(int Np, int Nt, int Nd, double Alpha, double Beta, double Frep, double Gamma, int& lastStep, vector<double> xMin, vector<double> xMax, double(*objFunc)(Array3D& ,int, int, int), string functionName, bool printResults=false, bool printPrettyResults=false) {

    double DeltaXi, bestFitness = -numeric_limits<double>::infinity(), deltaFrep = 0.1;
    double Denom, Numerator, SumSQ, DiagLength = 0;
    double positionTime = 0, correctionTime = 0, 
                fitnessTime  = 0, accelTime      = 0, 
                shrinkTime   = 0, convergeTime   = 0,
                totalTime    = 0;
    Array3D R(Np, Array2D(Nd, Array1D(Nt, 0)));
    Array3D A(Np, Array2D(Nd, Array1D(Nt, 0)));
    Array2D M(Np, Array1D(Nt, 0));
    Array1D bestFitnessArray;
    iArray1D bestProbeNumberArray(Nt);
    int bestTimeStep = 0, bestProbeNumber = 0;
    int numProbesPerAxis = Np / Nd;
    int numEvals = 0;
    string fileName;
    int numCorrections = 0;
    bool isCorrected;
    CStopWatch timer, timer1;

    timer1.startTimer();
    for (int i = 0; i < Nd; i++) {
        DiagLength += pow(xMax[i] - xMin[i], 2);
    }
    DiagLength = sqrt(DiagLength);

    // Initial Probe Distribution
    for (int i = 0; i < Nd; i++) {
        for (int p = 0; p < Np; p++) {
            R[p][i][0] = xMin[i] + Gamma * (xMax[i] - xMin[i]);
        }
    }

    for (int i = 0; i < Nd; i++) {
        DeltaXi = (xMax[i] - xMin[i]) / (numProbesPerAxis-1);
        int p;

        for (int k = 0; k < numProbesPerAxis; k++) {
            p = k + numProbesPerAxis * i;
            R[p][i][0] = xMin[i] + k * DeltaXi;
        }
    }

    // Set Initial Acceleration to 0
    for (int p = 0; p < Np; p++) {
        for (int i = 0; i < Nd; i++) {
            A[p][i][0] = 0;
        }
    }

    // Compute Initial  Fitness
    for (int p = 0; p < Np; p++) {
        M[p][0] = objFunc(R, Nd, p, 0);
        numEvals++;
    }

    // Time Steps
    lastStep = Nt;
    for (int j = 1; j < Nt; j++){
        // Compute new positions
        timer.startTimer();
        for (int p = 0; p < Np; p++) {
            for (int i = 0; i < Nd; i++) {
                R[p][i][j] = R[p][i][j - 1] + A[p][i][j - 1];
            }
        }
        timer.stopTimer();
        positionTime += timer.getElapsedTime();

        // Correct any errors
        timer.startTimer();
        for (int p = 0; p < Np; p++) {
            isCorrected = false;
            for (int i = 0; i < Nd; i++) {
                if (R[p][i][j] < xMin[i]) {
                    R[p][i][j] = max(xMin[i] + Frep * (R[p][i][j - 1] - xMin[i]), xMin[i]);
                    isCorrected = true;
                }
                if (R[p][i][j] > xMax[i]) {
                    R[p][i][j] = min(xMax[i] - Frep * (xMax[i] - R[p][i][j - 1]), xMax[i]);
                    isCorrected = true;
                }
            }
            if(isCorrected){ numCorrections++;}
        }
        timer.stopTimer();
        correctionTime += timer.getElapsedTime();

        // Update Fitness
        timer.startTimer();
        for (int p = 0; p < Np; p++) {
            M[p][j] = objFunc(R, Nd, p, j);
            numEvals++;

            if(M[p][j] >= bestFitness){
                bestFitness     = M[p][j];
                bestTimeStep    = j;
                bestProbeNumber = p;
            }
        }
        
        timer.stopTimer();
        fitnessTime += timer.getElapsedTime();

        // Compute next Acceleration
        timer.startTimer();
        #pragma omp parallel for
        for (int p = 0; p < Np; p++) { // Loop 1
            for (int i = 0; i < Nd; i++) { // Loop 2
                A[p][i][j] = 0;

                for (int k = 0; k < Np; k++) { // Loop 3
                    if (k != p) {
                        SumSQ = 0.0;

                        for (int L = 0; L < Nd; L++) {
                            SumSQ = SumSQ + pow(R[k][L][j] - R[p][L][j], 2);
                        }
                        if(SumSQ != 0){
                            Denom = sqrt(SumSQ);
                            Numerator = UnitStep((M[k][j] - M[p][j])) * (M[k][j]- M[p][j]);
                            A[p][i][j] = A[p][i][j] + (R[k][i][j] - R[p][i][j]) * pow(Numerator,Alpha)/pow(Denom,Beta);
                        }
                    }
                }
            }
        }
        timer.stopTimer();
        accelTime += timer.getElapsedTime();

        // Adjust Frep
        Frep += deltaFrep;
        if(Frep > 1){
             Frep = 0.05;
        }

        // Shrink Boundaries
        timer.startTimer();
        if(j % 10 == 0 && j >= 20){
            for(int i=0; i<Nd; i++){
                xMin[i] = xMin[i] + (R[bestProbeNumber][i][bestTimeStep] - xMin[i])/2.0;
                xMax[i] = xMax[i] - (xMax[i] - R[bestProbeNumber][i][bestTimeStep])/2.0;
            }

            // Correct any errors
            for (int p = 0; p < Np; p++) {
                isCorrected = false;
                for (int i = 0; i < Nd; i++) {
                    if (R[p][i][j] < xMin[i]) {
                        R[p][i][j] = max(xMin[i] + Frep * (R[p][i][j - 1] - xMin[i]), xMin[i]);
                        isCorrected = true;
                    }
                    if (R[p][i][j] > xMax[i]) {
                        R[p][i][j] = min(xMax[i] - Frep * (xMax[i] - R[p][i][j - 1]), xMax[i]);
                        isCorrected = true;
                    }
                }
                if(isCorrected){
                        numCorrections++;
                }
            }
        }
        timer.stopTimer();
        shrinkTime += timer.getElapsedTime();

        // Test for fitness saturation
        timer.startTimer();
        lastStep = j;
        if (hasFitnessSaturated(25, j, Np, Nd, M, R, DiagLength)) {
            timer.stopTimer();
            convergeTime += timer.getElapsedTime();
            break;
        }
        timer.stopTimer();
        convergeTime += timer.getElapsedTime();

    }//End Time Steps
    timer1.stopTimer();
    totalTime += timer1.getElapsedTime();

    Utils::recordValues(functionName, bestFitness, bestProbeNumber, bestTimeStep, Gamma, numProbesPerAxis, 
                        Np, Nd, lastStep, numEvals, numCorrections, positionTime, correctionTime, 
                        fitnessTime, accelTime, shrinkTime, convergeTime, totalTime);

    if(printResults){
        if(printPrettyResults){
            cout << setw(23) << left << "Function Name:" << functionName << endl;
            cout << setw(23) << left << "Best Fitness:" << setprecision(10) << bestFitness << endl;
            cout << setw(23) << left << "Number of Probes:" << Np << endl;
            cout << setw(23) << left << "Probes Per Axis:" << numProbesPerAxis << endl;
            cout << setw(23) << left << "Best Probe #:" << setprecision(0) << bestProbeNumber << endl;
            cout << setw(23) << left << "Best Gamma:" << Gamma << endl;     
            cout << setw(23) << left << "Best Time Step:" << bestTimeStep << endl;
            cout << setw(23) << left << "Last Step:" << setprecision(0) << lastStep << endl;
            cout << setw(23) << left << "Evaluations:" << setprecision(0) << numEvals << endl;
            cout << "-------------------------- Times --------------------------"            << endl;
            cout << setw(23) << left << "Position:"     << setprecision(4) << positionTime   << endl;
            cout << setw(23) << left << "Correction:"   << setprecision(4) << correctionTime << endl;
            cout << setw(23) << left << "Fitness:"      << setprecision(4) << fitnessTime    << endl;
            cout << setw(23) << left << "Acceleration:" << setprecision(4) << accelTime      << endl;
            cout << setw(23) << left << "Shrink:"       << setprecision(4) << shrinkTime     << endl;
            cout << setw(23) << left << "Converge:"     << setprecision(4) << convergeTime   << endl;
        }else{        
            cout    << functionName     << " "
                    << bestFitness      << " " 
                    << bestProbeNumber  << " "
                    << bestTimeStep     << " "
                    << Gamma            << " "
                    << numProbesPerAxis << " "
                    << Np               << " "
                    << Nd               << " "
                    << lastStep         << " "
                    << numEvals         << " "
                    << numCorrections   << " "
                    << positionTime     << " "
                    << correctionTime   << " "
                    << fitnessTime      << " "
                    << accelTime        << " "
                    << shrinkTime       << " "
                    << convergeTime     << " "
                    << totalTime        << endl;
        }
    }
}

void run(double oMin, double oMax, double (*rPtr)(Array3D&, int, int, int), string functionName){

    vector<double> xMin;
    vector<double> xMax;
    int lastStep;
    int Np, Nd, Nt;
    int NdMin = 20, NdMax = 50, NdStep = 10;
    double Alpha, Beta, Frep, Gamma, min, max;
    
    for(Nd=NdMin; Nd<=NdMax; Nd+=NdStep){ 
        Nt = 1000;  min = oMin; max = oMax;

        // Get Best Parameters
        MiniBenchmarkCFO(Np, Nd, Nt, Gamma, min, max, rPtr, functionName, true, true, false);

        for(int i=0; i<10;i++){
            Nt = 1000;
            xMin.clear(); xMin.resize(Nd, min);
            xMax.clear(); xMax.resize(Nd, max);
                    Alpha = 1; Beta = 2; Frep = 0.5;
                    standardCFO(Np, Nt, Nd,  Alpha, Beta, Frep, Gamma, lastStep, xMin, xMax, rPtr, functionName, true, false);
                }
        cout << endl;
    }
}

int main() {

    double min, max;
    double (*rPtr)(Array3D&, int, int, int) = NULL;   
    vector<double> xMin;
    vector<double> xMax;

    Utils::setupDb();

    cout << "Function, Fitness, Best Probe, Best Time Step, Gamma, NpNd, Np, Nd, Last Step, Evals, Corrections, Pos. Time, Cor. Time, Fit. Time, Accell. Time, Shrink Time, Conv. Time, Total Time\n";
        
    rPtr = &F1; min = -5.12; max = 5.12;
    run(min, max, rPtr, "F1");
    
    rPtr = &F2; min = -5.12; max = 5.12;
    run(min, max, rPtr, "F2");
    
    rPtr = &F3; min = -600; max = 600;
    run(min, max, rPtr, "F3");

    rPtr = &F4; min = -2.048; max = 2.048;
    run(min, max, rPtr, "F4");   

    rPtr = &F5; min = -32.768; max = 32.768;
    run(min, max, rPtr, "F5");

    rPtr = &F6; min = -5.12; max = 5.12;
    run(min, max, rPtr, "F6");

    rPtr = &F7; min = -10; max = 10;
    run(min, max, rPtr, "F7");   

    rPtr = &F8; min = -64; max = 64;
    run(min, max, rPtr, "F8"); 

    rPtr = &F9; min = -100; max = 100;
    run(min, max, rPtr, "F9"); 

    return 0;
}